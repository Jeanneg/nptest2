<!DOCTYPE html>
<html>
<body>

<h2>Test plot using <a href="https://www.chartjs.org/">Charts</a> grapher.</h2>

<div class="chart-container" style="height:60vh; width:60vw;margin: auto">
    <canvas id="myChart"></canvas>
</div>
<script src="dist/Chart.js"></script>
<script>
// Read the data from NP server and store it in a temp local file
function fetchData(dist){
	var httpText = new XMLHttpRequest();
	httpText.onreadystatechange = function() {
			var myObj = JSON.parse(this.responseText);
			localStorage.setItem("data" + dist, this.responseText);
		};
	httpText.open("GET", "data" + dist + ".txt", true);
	httpText.send();
}

fetchData("15")
data15 = JSON.parse(localStorage.getItem("data15"));
fetchData("25")
data25 = JSON.parse(localStorage.getItem("data25"));
localStorage.removeItem("data25");
fetchData("40")
data40 = JSON.parse(localStorage.getItem("data40"));
localStorage.removeItem("data40");
// Prepare data for plotting
x15 = [];
y15 = [];
for(var i in data15[0].data) {x15.push([data15[0].data[i].when]);y15.push(parseFloat(data15[0].data[i].value));}
y25 = [];
for(var i in data25[0].data) y25.push(parseFloat(data25[0].data[i].value));
y40 = [];
for(var i in data40[0].data) y40.push(parseFloat([data40[0].data[i].value]));

// Creat graph
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
    labels: x15,
    datasets: [{ 
        data: y15,
        label: data15[0].label.en,
        borderColor: "#0000ff",
		pointStyle: 'circle',
		radius: 1,
		hoverRadius: 5,
        fill: false
      },{ 
        data: y25,
        label: data25[0].label.en,
        borderColor: "#ff0000",
		pointStyle: 'rectRot',
		radius: 1,
		hoverRadius: 5,
        fill: false
      },
	  { 
        data: y40,
        label: data40[0].label.en,
        borderColor: "#00ff00",
		pointStyle: 'rect',
		radius: 1,
		hoverRadius: 5,
        fill: false
      }
    ]
  },
    options: {
        title: {
            display: true,
            text: 'Ground temperature in permafrost, Janssonhaugen'
        },
		legend: {
            display: true,
			position: 'bottom',
			usePointStyle: true
		},
		tooltips: {
			mode: 'index',
			intersect: false
		},
		hover: {
			mode: 'index',
			intersect: false
		},
        scales: {
            xAxes: [{
                ticks: {
					maxTicksLimit: 9
                }
            }],
            yAxes: [{
                ticks: {
					maxTicksLimit: 5,
                },
				scaleLabel: {
					display: true,
					labelString: data15[0].labels[0].label + ' (' + data15[0].unit.symbol + ')'
				}
            }]
        }
    }
});

</script>
</body>
</html>
